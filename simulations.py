#importing packages
import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks
import collections
#  plt.style.use('goodplot')
import numba
from functools import lru_cache
from collections import Counter
import sys
import matplotlib.patches as patches
import matplotlib.animation as animation
from matplotlib.animation import PillowWriter

# Defines the two functions used for simulating the system. Slightly modified
# froom the wikipedia version
@numba.njit
def f1( X,Y, m1):
    r1 = Y
    return r1
@numba.njit
def f2(X, Y, m1, m2):
    r2 = (m1-m2*X**2)*Y-X
    return r2

@numba.njit
def _numba_sim(omega, epsilon,epsilon_pertub,phi1, phi2, N, chaos, D):
    if chaos:
        x = 1+np.random.normal(0,0.01); y = 0+np.random.normal(0,0.01)
    else:
        x = 1; y = 0
    initial = [x, y]
    m1 = 0.6; m2 = 0.6
    m10 = 0.6
    m20 = 0.6
    dt = 0.01
    RT = 0
    Sav = np.zeros((N, 3))
    RT = 0
    for i in range(N):
        sig = 0
        disp = 0
        m1 = m10*(1 + epsilon*np.sin((omega)*RT+phi1))
        m2 = m20*(1 + epsilon_pertub*np.sin((omega+disp)*RT + phi2))

        ka1 = f1(x,y,m1)*dt
        ja1 = f2(x, y, m1, m2)*dt

        ka2 = f1(x + 0.5*ka1, y + 0.5*ja1, m1)*dt
        ja2 = f2(x + 0.5*ka1, y + 0.5*ja1, m1, m2)*dt

        ka3 = f1(x + 0.5*ka2, y + 0.5*ja2,m1)*dt
        ja3 = f2(x + 0.5*ka2, y + 0.5*ja2,m1, m2)*dt

        ka4 = f1(x + ka3, y + ja3, m1)*dt
        ja4 = f2(x + ka3, y + ja3,m1, m2)*dt

        x = x + 1./6*(ka1 + 2.*ka2 + 2.*ka3 + ka4) + np.sqrt(2*D*dt)*np.random.normal(0,1)
        y = y + 1./6*(ja1 + 2.*ja2 + 2.*ja3 + ja4) + np.sqrt(2*D*dt)*np.random.normal(0,1)


        RT += dt
        Sav[i,0] = RT
        Sav[i,1] = x
        Sav[i,2] = y
    return Sav#, initial#, dispersion 



# functions to determine the noise level and std for the tongues
def std_distance(D,N,  strength,per):
    trace = _numba_sim(2.0, strength, per, 0,np.pi, N, 1, D)
    inds, _ = find_peaks(trace[:, 1], height = 1, distance = 200)
    distance = np.diff(trace[inds, 0])
    return np.std(distance/ (2*np.pi/ 2.0)) , np.round(distance[:] / (2*np.pi/ 2.0), 2)

def running_mean(x, N):
    return np.convolve(x, np.ones(N)/N, mode='valid')
def mean_running(D, N, per):
    trace = _numba_sim(2.0, .8, per, 0, np.pi, N, 0, D)
    inds, _ = find_peaks(trace[:, 1], height = 1, distance = 200)
    distance = np.diff(trace[inds, 0])
    print(len(distance))
    run_mean = running_mean(distance, 10) / (2*np.pi/ 2.0)
    return run_mean

#  coup = mean_running(0.002, 1000000, 0.3)
#  non = mean_running(0.002, 1000000, 0.0)
#
#
#  plt.plot(non, label ='Single coupling', linewidth = 0.6)
#  plt.plot(coup, label = 'Double coupling', linewidth = 0.6)
#  plt.title('Running mean of 10 peaks')
#  plt.ylabel('Winding number')
#  plt.xlabel('Time [AU]')
#  plt.legend()
#  plt.show()
#  sys.exit()
#
#
#  #  #
#  val = np.linspace(0, 0.005, 1000)
#  std_bin = []
#  double_bin = []
#  std_2 = []
#  m1 = []
#  m2 = []
#  m3 = []
#  for value in val:
#      non = (std_distance(value, 500000, 0.6, 0))
#      coup = (std_distance(value, 500000, 0.6,0.2))
#      non_1 = std_distance(value, 500000, 0.0, 0.2)
#      std_bin.append(non[0])
#      m1.append(non[1])
#      double_bin.append(coup[0])
#      m2.append(coup[1])
#      std_2.append(non_1[0])
#      m3.append(non_1[0])
#  fig, plot = plt.subplots(1,1)
#  plot.plot(val, std_bin, linewidth = 0.7, label = r'$\mu_1$')
#  plot.plot(val, double_bin, linewidth = 0.7, label = r'$\mu_1 & \mu_2$')
#  plot.plot(val, std_2, linewidth = 0.7, label = r'$\mu_2$')
#  plot.set_xlabel('D')
#  plot.set_ylabel('STD')
#  plt.legend()
#  plt.tight_layout()
#  fig.savefig('Difussion_constant_low')
#

#This function simulates on ref frame with 1000 different points randomly scattered
#It does this inside of a refframe with side o1 -> o2 and e1->e2. Then it finds the
#last 8 peaks and calculate the period of the oscillation
def one_eps(eps_pertub, o1, o2, e1,e2, phi1, phi2, disp, D):
    freq = []
    amps = []
    for i in range(1000):
        ome = np.random.uniform(o1,o2)
        ak = np.random.uniform(e1,e2)
        k = _numba_sim(ome,ak,eps_pertub, phi1,phi2,50000, disp, D)
        inds, _ = find_peaks(k[:,1], height =1, distance = 200)
        inds = inds[len(inds)-8 :]
        fi0 = np.mean(np.diff(k[inds,0])) / (2*np.pi/ome)# Finds the winding number by deviding with the external frequency
        fi0 = np.round(fi0, 1)
        if fi0%1 == 0:
            amp = np.mean(k[inds,1])
        else:
            amp = np.nan
        freq.append(fi0)
        amps.append(amp)

    #Sorth the diffrent frequencies depending on how many times they appear 
    entries = Counter(freq)
    app = Counter(freq).most_common(1)
    val = Counter(freq).values()
    return entries , np.nanmean(amps)



def lyapunov(omega, eps, eps_p, N):
    x_val = np.zeros((5,N+1))
    for i in range(5):
        data, initial = _numba_sim(omega, eps, eps_p, 0,0, N, 1)
        x_val[i,1:] = data[:,1]
        x_val[i,0] = initial[0]
    dif_val = np.diff(x_val, axis = 0)
    lya_val = np.divide(abs(dif_val[:,0:]),abs(np.expand_dims(dif_val[:,0], axis=1)))
    mean_val = np.mean((lya_val[:,np.int(N//2):]), axis = 1)
    mean_val = np.mean(mean_val)
    if mean_val > 50:
        return_val = 10
    if mean_val < 1:
        return_val = 0
    else:
        return_val = 10
    return return_val
        

#  lyapunov(2.0, 0.5, 6.5, 100000)
## function to plot the lyapunov image ##
def lya_image(o1, o2, a1, a2, pertub):
    image = np.zeros((100, 500))
    omega = np.linspace(o1,o2, 500)
    amp = np.linspace(a1, a2, 100)
    for i in range(100):
        print(1*i, '%')
        for j in range(500):
            image[i,j] = lyapunov(omega[j], amp[i], pertub, 50000)
    plt.imshow(np.flipud(image),aspect = 'auto', cmap ='binary', extent = [o1,o2,a1,a2])
    plt.xlabel(r'$\omega$')
    plt.ylabel(r'$\epsilon$')
    plt.title(r'$\Lambda$ diagram')
    plt.savefig('lyapunov_with_axes')
    plt.tight_layout()
    plt.show()

#  lya_image(0,5,1,6, 0.5)
        


def arnold_plot(o1,o2,ak1,ak2, pertub, phi1, phi2):
    ome = np.linspace(o1,o2,500)
    ak = np.linspace(ak1,ak2,40)
    arnold = np.zeros((40,500))
    for i0 in range(500):
        #  print(i0)
        for iA in range(40):
            k = _numba_sim(ome[i0],ak[iA], pertub, phi1, phi2 ,100000, 0, 0)
            inds, _ = find_peaks(k[:,1])
            inds = inds[len(inds)-10 :]
            fi0 = np.mean(np.diff(k[inds,0])) / (2*np.pi/ome[i0])# Finds the winding number by deviding with the external frequency
            fi0 = np.round(fi0, 1)
            if fi0%1 != 0:
                fi0 = 0
            arnold[iA,i0] = fi0
    return arnold

    
## helper functio for the phase test and the noise test    
def loop_over_many(noise):

    #  omega1 = [2.1, 4.0, 0.8]
    #  omega2 = [2.4, 4.3, 1.1]
    #  eps1 = [0.5, 0.7, 1.0]
    #  eps2 = [0.8, 1.0, 1.3]
    #  omega1 = omega1[noise]
    #  omega2 = omega2[noise]
    #  eps1 = eps1[noise]
    #  eps2 = eps2[noise]
    omega1 = np.random.uniform(0,4.5)
    omega2 = omega1 + 0.5 #np.random.uniform(omega1,omega1+2)
    eps1 = np.random.uniform(0,1)
    eps2 = eps1 + 0.3 #np.random.uniform(eps1,eps1+0.5 )
    param = [omega1, omega2, eps1, eps2]
    plot_box_2 = np.zeros(50)
    plot_box_4 = np.zeros(50)
    check = []
    plot_box_2_control = np.zeros(100)
    #  plot_box_4_control = np.zeros(50)
    check_control = []
    e_p = np.linspace(0, 2*np.pi, 50)
    amps_box = []
    ep = np.linspace(0,1.0,50)
    #  res_c , c_amp= one_eps(0.0, omega1, omega2, eps1, eps2, 0,0, 0, 0)
    #  c_val = (res_c[3.0]/1000*100) + (res_c[2.0]/1000*100) + (res_c[1.0]/1000*100)+ (res_c[4.0]/1000*100)+(res_c[5.0]/1000*100)
    #  if c_val == 0:
    #      print('Problem')
    #      sys.exit()
    for i in range(50):
        #  print(i)
        res, amp = one_eps(ep[i], omega1, omega2, eps1, eps2, 0, np.pi, 0, 0.003)
        plot_box_2[i] = ((res[1.0]/1000*100) + (res[2.0]/1000*100) + (res[3.0]/1000*100)+ (res[4.0]/1000*100)+(res[5.0]/1000*100))#/c_val
        res, amp = one_eps(ep[i], omega1, omega2, eps1, eps2, 0, np.pi, 0, 0.00)
        plot_box_4[i] = ((res[1.0]/1000*100) + (res[2.0]/1000*100) + (res[3.0]/1000*100)+ (res[4.0]/1000*100)+(res[5.0]/1000*100))#/c_val
        amp_ratio = amp#/c_amp
        amps_box.append(amp_ratio)
        #  res, app ,amp= one_eps(0.2, omega1[1], omega2[1], eps1[0], eps2[1], e_p[i], 0, 0)
        #  plot_box_4[i] = ((res[2.01]/1000*100) + (res[2.0]/1000*100) + (res[1.99]/1000*100)+ (res[4.0]/1000*100)+(res[5.0]/1000*100))/c_val
        #  amp_ratio = amp/amp_c
        #  amps2_box.append(amp_ratio)
        #  plot_box_4[i] = (res[4.0]/1000*100)
        
        #  check.append(res[1.0])
        #  check.append(res[2.0])
    return param, plot_box_2, amps_box, plot_box_4 

def noise_test():
    return_array = np.zeros((10,50))
    return_2 = np.zeros((10,50))
    for i in range(10):
        _, return_array[i,:], __, return_2[i,:] = loop_over_many(0)
    np.savetxt('with_noise_small.txt', return_array, delimiter = ',')
    np.savetxt('without_noise_same0001.txt', return_2, delimiter = ',')
#  noise_test()
#  sys.exit()

## Phase test results ##
def phase_test():
    _, one_val, amp_one= loop_over_many(2)
    _, two_val, amp_two= loop_over_many(0)
    _, four_val, amp_four= loop_over_many(1)
    fig, plot = plt.subplots(1,2, figsize = (14,7))
    phase = np.linspace(0, 2*np.pi, 50)


    amp_val = np.zeros((3,50))
    ml_val = np.zeros((3,50))
    amp_val[0,:] = amp_one
    amp_val[1,:] = amp_two
    amp_val[2,:] = amp_four
    ml_val[0,:] = one_val
    ml_val[1,:] = two_val
    ml_val[2,:] = four_val
    np.savetxt('amp_val_eps.txt', amp_val, delimiter = ',')
    np.savetxt('ml_val_eps.txt', ml_val, delimiter = ',')

#  phase_test()


## Plot the rough outline of the 2/1 tounge ##

#  from matplotlib.colors import ListedColormap
#
#  colors = ListedColormap(['white','red', 'blue', 'green', 'teal', 'black'], name='from_list', N=None)
#  pertu = np.array([0,0.5,1, 1])
#  for i in range(4):
#      data = arnold_plot(0,5,0,2,pertu[i],0,np.pi)
#      data = np.where(data == 2, 1, 0)
#      data_pad = np.pad(data, (1,1), 'minimum')
#      mask = np.where(data == 1)
#      copy = np.zeros_like(data_pad)
#      for j in range(len(mask[0])):
#          up_down = data_pad[mask[0][j]-1,mask[1][j]] - data_pad[mask[0][j]+1,mask[1][j]]
#          l_r = data_pad[mask[0][j],mask[1][j]-1] - data_pad[mask[0][j],mask[1][j]+1]
#          if up_down != 0 or l_r != 0:
#              copy[mask[0][j],mask[1][j]] = (i+1)*10# np.nan
#      if i == 0:
#          final = copy+2
#      else:
#          final = final + copy
#
#  plt.imshow(np.flipud(final), aspect = 'auto', cmap = 'binary', extent = [0,5,0,2])
#  plt.colorbar()
#
#  plt.savefig('edge_comparison_22')
#  plt.show()
#  sys.exit()
#

## To plot the bounding box/ Arnold tongue diagram##


#  val = np.linspace(0,1,50)
#  val = [0.0, 0.5]
#  fig, plot = plt.subplots(1,1, figsize = (7,7))
#  #  val = np.linspace(-2,3,50)
#  for i in range(1):
#      data = arnold_plot(0,5,0,2,0.0,0, 0)
#      rect = patches.Rectangle((2.,0.5),0.3,0.3,linewidth=1,edgecolor='red',facecolor='none')
#      plot.add_patch(rect)
#      rect = patches.Rectangle((3.9,0.7),0.3,0.3,linewidth=1,edgecolor='red',facecolor='none')
#      plot.add_patch(rect)
#      rect = patches.Rectangle((0.8,1.0),0.3,0.3,linewidth=1,edgecolor='red',facecolor='none')
#      plot.add_patch(rect)
#      plot.imshow(np.flipud(data), aspect = 'auto', cmap ='binary', extent = [0,5,0,2])
#      plot.set_title(f'Bounding box placement ')
#      plot.set_ylabel(r'$\epsilon$')
#      plot.set_xlabel('W')
#      plt.savefig('Bounding_box_placement_new')
#      plt.show()
#      plt.clf()
#
#      #  plt.savefig(f'./disp_test/test_{i}')
#


## PLot the Devils staircase figure ##

def devils_staircase(o1,o2,ak1,pertub, phi1, phi2, disp):
    ome = np.linspace(o1,o2,5000)
    bin_12 = []
    amp_bin = []
    for i0 in range(5000):
        k12 = _numba_sim(ome[i0],ak1,pertub, phi1, phi2, 100000, disp, 0)
        inds, _ = find_peaks(k12[:,1], height = 0)
        inds = inds[len(inds)-8 :]
        fi0 = np.mean(np.diff(k12[inds,0])) / (2*np.pi/(ome[i0]))# Finds the winding number by deviding with the external frequency
        bin_12.append(fi0)
    return np.asarray(bin_12), amp_bin
fig, plot = plt.subplots(1,1, figsize = (10,7))
omega_value = np.linspace(1,5,5000)
one_osc, amp1 = devils_staircase(1,5,0.5, 0., 0 ,0, 0)
both_osc, amp12 = devils_staircase(1,5,0.5, 0.2, 0 ,0, 0)
two_osc, amp2 = devils_staircase(1,5,0.0, 0.2, 0, 0, 0)
#  print(one_osc)
plot.axvspan(1.83, 2.07, 0,6, alpha = 0.2, color = 'blue')
plot.axvspan(1.78, 2.11, 0,6, alpha = 0.3, color = 'orange')
plot.axvspan(3.78, 4.03, 0,6, alpha = 0.2, color = 'blue')
plot.axvspan(3.86, 3.96, 0,6, alpha = 0.3, color = 'orange')
plot.plot(omega_value, two_osc+1, label = r'$\mu_2$')
plot.plot(omega_value, one_osc-1, label = r'$\mu_1$')
#  plot.plot(omega_value, amp1, label = r'$\mu_2$')
plot.plot(omega_value, both_osc, label = r'$\mu_1 \, & \,  \mu_2$')
plot.set_xlabel(r'$\omega$')
plot.set_ylabel(r'$\Omega$')
plot.legend()
plot.set_title('Devils staircase')
plt.tight_layout()
fig.savefig('Devislstaircase_example')
plt.show()
plt.clf()
